const std = @import("std");
const zl  = @import("zl.zig");

pub export fn _start() noreturn { // callconv(.Naked)
    defer std.os.exit(0);

    const client = try zl.Client.init();
    defer client.deinit();
}
