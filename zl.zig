const std = @import("std");
const mem = std.mem;
const os  = std.os;

pub const Client = struct {
    sock: os.socket_t,

    inline fn initSock() !os.socket_t {
        const sock = try os.socket(os.AF.UNIX, os.SOCK.STREAM | os.SOCK.CLOEXEC, 0); // 0 = os.IPPROTO.IP
        errdefer deinitSock(sock);
        var sock_addr = comptime os.sockaddr.un {
            .family = os.AF.UNIX,
            .path   = undefined,
        };
        // mem.zeroInit()
        comptime sock_addr.path = [_]u8 { 0 } ** sock_addr.path.len; // mem.set(u8, &sock_addr.path, 0);
        // https://github.com/Aransentin/ZWL/blob/master/src/wayland.zig#L103-%23L104
        // const XDG_RUNTIME_DIR = os.getenv("XDG_RUNTIME_DIR") orelse return error.XDGRuntimeDirNotSpecified;
        // const WAYLAND_DISPLAY = os.getenv("WAYLAND_DISPLAY") orelse "wayland-0";
        // mem.copy(u8, &sock_addr.path, );
        try os.connect(sock, @ptrCast(*const os.sockaddr, &sock_addr), @sizeOf(os.sockaddr.un)); // comptime
        return sock;
    }

    inline fn deinitSock(sock: os.socket_t) void {
        os.closeSocket(sock);
    }

    pub fn init() !Client {
        return Client { .sock = try initSock() };
    }

    pub fn deinit(self: Client) void {
        deinitSock(self.sock);
    }
};

pub const Server = struct {};
